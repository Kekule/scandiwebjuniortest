<?php

require_once PAGES_DIR . DS . 'products' . DS . 'model.php';

/**
 * parent of all product types
 */
abstract class ProductType
{

    /**
     * @var Product $product
     */
    public $product;

	/**
	 * @return array - collection of names of all product type class names
	 */
    public static function getAllTypes () {
        $folder = PAGES_DIR . DS . 'products' . DS . 'product_types';
        $files = scandir($folder);
        $result = [];
        foreach ($files as $k => $v) {
            if (is_file($folder . DS . $v)) {
                $result[] = explode('.php', $v, 2)[0];
            }
        }
        return $result;
    }

	/**
	 * return html code, that renders form for every product type
	 * @return string
	 */
    public static function renderAllForm () {
        $types = self::getAllTypes();

        ob_start();
    ?>
        <div class="form-group">
            <label for="type" class="form-label">Type of product</label>
            <select id="type" name="type" class="form-element" onchange="showTypeForm()">
                <?php foreach($types as $type): ?>
                    <?php if (self::getType($type)): ?>
                        <option <?= old('type') == $type ? 'selected' : '' ?>><?= $type ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="all-types">
        <?php foreach ($types as $type): ?>
            <div class="type" data-type="<?= $type ?>">
                <?= self::getType($type)->renderForm(); ?>
            </div>
        <?php endforeach; ?>
        </div>
    <?php
        return ob_get_clean();
    }

    /**
     * get instance of $type or null if such class doesn't exist
     * @param string $type - class name to find
     * @return mixed - instance of $type on success, null otherwise
     */
    public static function getType ($type) {
        $folder = PAGES_DIR . DS . 'products' . DS . 'product_types';
        $file = $folder . DS . $type . '.php';
        if (file_exists($file)) {
            require_once $file;
            $class = ucfirst($type);
            if (class_exists($class)) {
                return new $class;
            }
        }
        return null;
    }

	/**
	 * @return string js code required for product type
	 */
    public function addJS () {
        return '';
    }

	/**
	 * @return string id generated for input name
	 */
    public function getInputID ($name) {
        $class = get_class($this);
        return "attribs_{$class}_$name";
    }

	/**
	 * @return string generated input name
	 */
    public function getInputName ($name) {
        return "attribs[$name]";
    }

    public abstract function renderForm ();
    public abstract function renderGridItem ();
    public abstract function getRules ();
    public abstract function getMessages ();
    public abstract function getJSON ();

}
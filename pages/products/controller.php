<?php

defined('EXEC') or die('Restricted access');

require_once LIB_DIR . DS . 'controller.php';
require_once LIB_DIR . DS . 'view.php';
require_once LIB_DIR . DS . 'validator.php';
require_once __DIR__ . DS . 'ProductType.php';
require_once __DIR__ . DS . 'model.php';

class ProductsController extends Controller
{

	/**
	 * default task that will be displayed if invalid task is requested
	 */
    public function index () {
        View::displayOrDie(__DIR__ . DS . 'views' . DS . 'index.php', [
            'title' => 'Product List',
            'products' => Product::all()
        ]);
    }

	/**
	 * displays create form to add new product
	 */
    public function create () {
		$old = c('old', $_SESSION);
        View::displayOrDie(__DIR__ . DS . 'views' . DS . 'create.php', [
            'title' => 'Product New',
            'headerTitle' => 'Product Add',
            'product' => new Product($old)
        ]);
    }

	/**
	 * stores new product
	 * validates input
	 * redirects back if validation fails
	 */
    public function store () {
        unset($_POST['id']);

        $type = c('type', $_POST);
        $productType = ProductType::getType($type);
        if ($productType == null) {
            $_SESSION['messages'] = ['type' => 'invalid product type'];
            $this->redirect('/index.php?page=products');
        }
        $rules = [
            'name' => 'required|string|min:3',
            'price' => 'required|numeric|min:0',
            'sku' => 'required|string|min:3|unique:products'
        ];
        $rules += $productType->getRules();
        $validator = new Validator($rules, $_POST, $productType->getMessages());

        if ($validator->fails()) {
            $_SESSION['old'] = $_POST;
            $_SESSION['messages'] = $validator->getMessages();
            $this->redirect('/index.php?page=products&task=create');
        } else {
            $product = Product::fromArray($_POST);
            $product->attribs = $productType->getJSON();
            $product->save();

            unset($_SESSION['old']);
            unset($_SESSION['messages']);
            $this->redirect('/index.php?page=products');
        }
    }

	/**
	 * deletes all of the ticked items from list
	 */
    public function massDelete () {
        $ids = c('ids', $_POST);
        if (is_string($ids)) {
            $arr = explode('|', $ids);
            Product::massDelete($arr);
        }
        $this->redirect('/index.php?page=products');
    }
}
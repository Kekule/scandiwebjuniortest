<?php

require_once PAGES_DIR . DS . 'products' . DS . 'ProductType.php';

class Furniture extends ProductType
{
	/**
	 * @return string html code for rendering form
	 */
    public function renderForm () {
        $id = $this->getInputID('dimensions');
        $name = $this->getInputName('dimensions');
        ob_start();
    ?>
        <div class="form-group">
            <label for="<?= $id ?>" class="form-label">Dimensions in HxWxL</label>
            <input type="text" id="<?= $id ?>" name="<?= $name ?>" class="form-element" value="<?= old('attribs.dimensions') ?>" placeholder="WxHxL" />
        </div>
        <p class="description">
            Please provide dimensions in HxWxL format
        </p>
    <?php
        return ob_get_clean();
    }

	/**
	 * @return string html code to display grid item
	 */
    public function renderGridItem () {
        ob_start();
    ?>
        <span>Dimensions : <?= c('dimensions', $this->product->attribs) ?></span><br>
    <?php
        return ob_get_clean();
    }

	/**
	 * @return array custom rules for Book type
	 */
    public function getRules () {
        return [
            'attribs.dimensions' => 'required|regex:/^\d{1,}x\d{1,}x\d{1,}$/'
        ];
    }

	/**
	 * @return array custom error messages
	 */
    public function getMessages () {
        return [
            'attribs.dimensions.required' => 'dimensions must be filled',
            'attribs.dimensions.regex' => 'dimensions in invalid format'
        ];
    }

	/**
	 * @return string json to save in db
	 */
    public function getJSON () {
        $array = [
            'dimensions' => c('attribs.dimensions', $_POST)
        ];
        return json_encode($array);
    }
}
<?php

require_once PAGES_DIR . DS . 'products' . DS . 'ProductType.php';

class DVD extends ProductType
{
	/**
	 * @return string html code for rendering form
	 */
    public function renderForm () {
        $id = $this->getInputID('size');
        $name = $this->getInputName('size');
        ob_start();
    ?>
        <div class="form-group">
            <label for="<?= $id ?>" class="form-label">Size in MB</label>
            <input type="number" id="<?= $id ?>" name="<?= $name ?>" class="form-element" min="1" value="<?= old('attribs.size') ?>" />
        </div>
        <p class="description">
            Please enter capacity of DVD in MB.
        </p>
    <?php
        return ob_get_clean();
    }

	/**
	 * @return string html code to display grid item
	 */
    public function renderGridItem () {
        $size = c('size', $this->product->attribs);
        ob_start();
    ?>
        <span>Size : <?= number_format($size) ?> Mb.</span><br>
    <?php
        return ob_get_clean();
    }

	/**
	 * @return array custom rules for Book type
	 */
    public function getRules () {
        return [
            'attribs.size' => 'required|numeric'
        ];
    }

	/**
	 * @return array custom error messages
	 */
    public function getMessages () {
        return [
            'attribs.size.required' => 'size feild must be filled',
            'attribs.size.numerici' => 'size field must be unique'
        ];
    }

	/**
	 * @return string json to save in db
	 */
    public function getJSON () {
        $array = [
            'size' => c('attribs.size', $_POST)
        ];
        return json_encode($array);
    }
}
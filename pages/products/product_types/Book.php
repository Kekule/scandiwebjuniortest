<?php

require_once PAGES_DIR . DS . 'products' . DS . 'ProductType.php';

class Book extends ProductType
{
	/**
	 * @return string html code for rendering form
	 */
    public function renderForm () {
        $id = $this->getInputID('weight');
        $name = $this->getInputName('weight');
        ob_start();
    ?>
        <div class="form-group">
            <label for="<?= $id ?>" class="form-label">Wight in Kg</label>
            <input type="number" id="<?= $id ?>" name="<?= $name ?>" min="1" value="<?= old('attribs.weight') ?>" class="form-element" />
        </div>
        <p class="description">
            Please enter weight of book in Kg.
        </p>
    <?php
        return ob_get_clean();
    }

	/**
	 * @return string html code to display grid item
	 */
    public function renderGridItem () {
        $weight = c('wheight', $this->product->attribs);
        ob_start();
    ?>
        <span>weight : <?= number_format($weight) ?> Kg.</span><br>
    <?php
        return ob_get_clean();
    }

	/**
	 * @return array custom rules for Book type
	 */
    public function getRules () {
        return [
            'attribs.weight' => 'required|numeric'
        ];
    }

	/**
	 * @return array custom error messages
	 */
    public function getMessages () {
        return [
            'attribs.weight.required' => 'weight must be filled',
            'attribs.weight.numeric' => 'weight must be numeric'
        ];
    }

	/**
	 * @return string json to save in db
	 */
    public function getJSON () {
        $array = [
            'weight' => c('attribs.weight', $_POST, null)
        ];
        return json_encode($array);
    }
}
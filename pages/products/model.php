<?php

require_once LIB_DIR . DS . 'db.php';

class Product
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $sku;
	
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var int
	 */
	public $price;
	
	/**
	 * @var string
	 */
	public $type;
	
	/**
	 * @var array
	 */
    public $attribs;

	/**
	 * @param int $id
	 * @param string $sku
	 * @param string $name
	 * @param int $price
	 * @param string $type
	 * @param mixed $attribs
	 */
    public function __construct ($id = null, $sku = '', $name = '', $price = 1, $type = '', $attribs = null) {
        $this->id = (int)trim($id);
        $this->sku = trim(($sku));
        $this->name = trim($name);
        $this->price = (int)trim($price);
        $this->type = $type;
        if (is_string($attribs)) {
            $attribsArray = json_decode($attribs);
            $this->attribs = $attribsArray;
        }
    }

	/**
	 * creates Product from array and returns it
	 * @return Product
	 */
    public static function fromArray ($array) {
        $id = c('id', $array, null);
        $sku = c('sku', $array, '');
        $name = c('name', $array, '');
        $price = c('price', $array, 0.01) * 100;
        $type = c('type', $array);
        $attribs = c('attribs', $array, '');
        return new Product(trim($id), trim($sku), trim($name), trim($price), trim($type), trim($attribs));
    }

	/**
	 * returns array of found products
	 * @param string $column
	 * @param string $value
	 * @param string $operator
	 * @return array collection of found products
	 */
    public static function findByColumn ($column, $value, $operator = '=') {
        $pdo = DB::getPDO();
        $stmt = $pdo->prepare('SELECT * FROM products WHERE ' . $column . ' ' . $operator . ' :val');
        $stmt->execute([
            'val' => $value
        ]);
        $result = [];
        
        while ($row = $stmt->fetch()) {
            $result[] = new Product($row['id'], $row['sku'], $row['name'], $row['price'], $row['type'], $row['attribs']);
        }
        return $result;
    }

	/**
	 * @return array array of all products
	 */
    public static function all () {
        $pdo = DB::getPDO();
        $stmt = $pdo->query('SELECT * FROM products');
        $result = [];
        while ($row = $stmt->fetch()) {
            $result[] = new Product($row['id'], $row['sku'], $row['name'], $row['price'], $row['type'], $row['attribs']);
        }
        return $result;
    }

	/**
	 * deletes all records where id is in $arr
	 * @param array $ids
	 */
    public static function massDelete ($ids) {
        if (is_array($ids)) {
            $arr = array_filter($ids, function ($item) {
                return is_numeric($item);
            });
            $in = implode(', ', $arr);
            if ($in) {
                $pdo = DB::getPDO();
                $pdo->query('DELETE FROM products WHERE id in (' . $in . ')')->execute();
            }
        }
    }

	/**
	 * saves product in database
	 * if id is not set creates new record,
	 * if id is set updates existing one
	 * @return int number of rows affected (should always be 1)
	 * @throws Exception query execution was not successfull
	 */
    public function save () {
        $pdo = DB::getPDO();
        if ($this->id) {
            $stmt = $pdo->prepare('UPDATE products SET `name` = :n sku = :sku price = :price `type` = :t attribs = :attribs WHERE id = :id');
        } else {
            $stmt = $pdo->prepare('INSERT INTO products(`name`, sku, price, `type`, attribs) VALUES(:n, :sku, :price, :t, :attribs)');
        }
        $result = $stmt->execute([
            // ':id' => $this->id,
            ':n' => $this->name,
            ':sku' => $this->sku,
            ':price' => $this->price,
            ':attribs' => $this->attribs,
            ':t' => $this->type
        ]);

        if (!$result) {
            $error = $stmt->errorInfo();
            $msg = ['Failed to execute query'];
            foreach ($error as $e) {
                $msg[] = $e;
            }
            throw new Exception(implode(' ', $msg), 1);
        }
        return $result;
    }

	/**
	 * return price formatted using php's number_format
	 * @param int $decimals
	 * @param string $dec_points
	 * @param string $thousands_sep
	 * @see number_format
	 */
    public function formatPrice ($decimals = 2, $dec_points = '.', $thousands_sep = ',') {
        return number_format($this->price / 100, $decimals, $dec_points, $thousands_sep);
    }

}
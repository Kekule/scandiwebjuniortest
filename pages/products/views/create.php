<?php
include View::partial('head.php');
?>

<header class="header-container">
    <h1 class="header"><?= $headerTitle ?><h1>
    <div class="buttons-right">
        <a type="submit" value="submit" onclick="submitForm()" class="btn">Submit</a>
    </div>
    <hr>
</header>
<?php
foreach ($_SESSION['messages'] as $message) {
    foreach ($message as $m) {
    ?>
        <p class="error-msg"><?= $m ?></p><br>
    <?php
    }
}
?>
<form id="form" method="POST" action="/index.php?task=store&page=products">
    <div class="form-group">
        <label for="name" class="form-label">Name</label>
        <input name="name" id="name" type="text" class="form-element" value="<?= old('name', '') ?>" autocomplete="off" />
    </div>
    <div class="form-group">
        <label for="sku" class="form-label">SKU</label>
        <input name="sku" id="sku" type="text" class="form-element" value="<?= old('sku', '') ?>" autocomplete="off" />
    </div>
    <div class="form-group">
        <label for="price" class="form-label">Price</label>
        <input name="price" id="price" type="number" class="form-element" min="0.01" step="0.01" value="<?= old('price', 0.01) ?>" />
    </div>

    <?php
        echo ProductType::renderAllForm();
    ?>
</form>

<?php
include View::partial('footer.php');
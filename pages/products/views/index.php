<?php
include View::partial('head.php');
?>

<header class="header-container">
    <h1 class="header"><?= $title ?></h1>
    <div class="buttons-right">
        <a class="btn" onclick="submitMassDelete();">Mass Delete</a>
        <form style="display: none" id="massdelete_form" method="POST" action="/?page=products&task=massDelete">
            <input id="ids" name="ids" type="hidden" />
        </form>
        <a class="btn" href="/?page=products&task=create">Create New</a>
    </div>
    <hr/>
</header>

<div class="products-list">
    <?php foreach ($products as $p): ?>
        <div class="product-item">
            <input type="checkbox" class="cb" id="cb_<?= $p->id ?>" onchange="addToMassDelete(event)" />
            <span>
                <span><?= $p->sku ?></span><br>
                <span><?= $p->name ?></span><br>
                <span><?= $p->formatPrice() ?>$</span><br>
                <?php
                    $type = ProductType::getType($p->type);
                    $type->product = $p;
                    echo $type->renderGridItem();
                ?>
            <span>
        </div>
    <?php endforeach; ?>
</div>

<?php
include View::partial('footer.php');
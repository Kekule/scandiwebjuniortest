function showTypeForm () {
    let selectedtype = $('#type');
    let form_to_show = selectedtype.val();
    let typeforms = $('.type');
    typeforms.each(function () {
        if ($(this).attr('data-type') == form_to_show) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });
}

document.body.onload = function () {
    showTypeForm();
}

function submitForm () {
    $('#form').submit();
}

var massDelete = [];

function addToMassDelete (event) {
    let elem = event.srcElement;
    let id = elem.id.substring(3, elem.id.length);
    if (elem.checked) {
        massDelete.push(id);
    } else {
        massDelete = massDelete.filter(function (value, index, array) {
            return value != id;
        });
    }
}

function submitMassDelete () {
    let ids = massDelete.join('|');
    $('#ids').val(ids);
    $('#massdelete_form').submit();
}
**Instructions**

in .config file set correct values for your environment
db - database driver
dbName - name of database
dbUser - user of database
dbPass - password for dbUser
host - host to use in dsn string
fallbackPage - default page (leave as is)

after that run php -S localhost:8000

after setting all variables and starting server run initdb.php script (either run from terminal "php initdb.php" or visit /initdb.php from browser)

start using site :)
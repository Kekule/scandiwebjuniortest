<?php

define('EXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

require_once __DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'config.php';

$config = Config::getInstance();

$query = file_get_contents(__DIR__ . '/mb.sql');
$query = str_replace('${dbName}', $config->dbName, $query);

$queries = explode(';', $query);

$host = $config->host;
$user = $config->dbUser;
$pass = $config->dbPass;

try {
    $pdo = new PDO("mysql:host=$host;", $user, $pass);
} catch (PDOExcepton $e) {
    print_r($e->getMessage());
    die;
}

foreach ($queries as $q) {
    $pdo->exec($q);
}
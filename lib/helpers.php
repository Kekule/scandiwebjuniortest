<?php

defined('EXEC') or die('Restricted access');

/**
 * collection of helper functions
 */

/**
 * get value from $collection or $default
 * @return mixed value at index $key if set, or $default
 */
function c ($key, $collection, $default = null) {
    $keys = explode('.', $key);
    foreach ($keys as $key) {
        if (is_object($collection)) {
            $collection = (array)$collection;
        }
        
        if (is_array($collection) && isset($collection[$key])) {
            $collection = $collection[$key];
            continue;
        } else {
            return $default;
        }
    }
    return $collection;
}

/**
 * get previously filled value form session
 * @return mixed old value at index $key if set, or $default
 */
function old ($key, $default = null) {
    return c('old.' . $key, $_SESSION, $default);
}

/**
 * get error message from session
 * @return array returns assoc array of messages, where keys are name of fields
 */
function messages ($key, $default = null) {
    return c('messages.' . $key, $_SESSION, $default);
}
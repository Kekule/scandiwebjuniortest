<?php

defined('EXEC') or die('Restricted access');

require_once LIB_DIR . DS . 'db.php';

/**
 * class for easily validating input
 */
class Validator
{
	/**
	 * @var array rules to validate against input
	 */
	public $rules = [];

	/**
	 * @var array input to validate
	 */
	public $input = [];

	/**
	 * @var array custom messages to use when generating errors
	 */
    public $messages = [];

	/**
	 * @var array generated messages
	 */
    private $messageBag = [];

	/**
	 * consturcts Validator class
	 */
    public function __construct ($rules, $input, $messages = []) {
        $this->rules = $rules;
        $this->input = $input;
        $this->messages = $messages;
    }

	/**
	 * validates input and keeps generated messages in $this->messageBag
	 * @return Validator $this to allow chaining
	 */
    public function validate () {
        foreach ($this->rules as $k => $v) {
            $validationRules = explode('|', $v);
            foreach ($validationRules as $rule) {
                try {
                    $this->checkRule($k, $rule);
                } catch (Exception $e) {
                    echo $e->getMessage();
                    die;
                }
            }
        }
        return $this;
    }

	/**
	 * validtes input and returns number of inputs that failed validating
	 * useful in conditional statements
	 * @return int number of inputs that failed to validate
	 */
    public function fails () {
        $this->validate();
        return count($this->messageBag) != 0;
    }

	/**
	 * @return array assoc array of generated messages, where keys are input names
	 */
    public function getMessages () {
        return $this->messageBag;
    }

	/**
	 * checks rule against input
	 * @return bool
	 * @throws Exception if rule is not defined
	 */
    private function checkRule ($input, $rule) {
        $functionAndArgs = explode(':', $rule, 2);
        $function = $functionAndArgs[0];
        $args = isset($functionAndArgs[1]) ? $functionAndArgs[1] : '';
        if (method_exists($this, $function)) {
            return $this->{$function}($input, $args);
        } else {
            throw new Exception("Undefined rule $function");
        }
    }

	/**
	 * adds message to messageBag
	 * takes message from custom messages, if not set takes $default message
	 * @return void
	 */
    public function addMessage ($input, $rule, $default) {
        $key = $input . '.' . $rule;
        $message = isset($this->messages[$key]) ? $this->messages[$key] : $default;
        print_r([
            'key' => $key,
            'messages' => $this->messages
        ]);
        $this->messageBag[$input][$rule] = $message;
    }

	/**
	 * rule to check that input is set
	 * input is trimmed first
	 * @return bool
	 */
    public function required ($input) {
        $result = c($input, $this->input);
        if (!$result) {
            $this->addMessage($input, 'required', "$input must be filled");
        }
        return (bool)$result;
    }

	/**
	 * check if input is set and is numeric
	 * @return bool
	 */
    public function numeric ($input) {
        $result = is_numeric(c($input, $this->input));
        if (!$result) {
            $this->addMessage($input, 'numeric', "$input must be numeric");
        }
        return $result;
    }

    /**
     * checks if suck input is already set in db or not
	 * @return bool
     */
    public function unique ($input, $args) {
        if (c($input, $this->input) == null) {
            return false;
        }

        $args = explode(',', $args);
        $table = $args[0];
        //if column isn't specify than use $input as column name
        $column = c(1, $args, $input);

        $pdo = DB::getPDO();
        $stmt = $pdo->prepare("SELECT $column FROM $table WHERE $column = :val");
        $stmt->execute([':val' => $this->input[$input]]);
        $result = count($stmt->fetchAll()) == 0;
        if (!$result) {
            $this->addMessage($input, 'unique', "$input must be unique");
        }
        return $result;
    }

	/**
	 * check if input is string
	 * @return bool
	 */
    public function string ($input) {
        $result = is_string(c($input, $this->input));
        if (!$result) {
            $this->addMessage($input, 'string', "$input is not a string");
        }
        return $result;
    }

	/**
	 * if input is numeric checks if its values is less than or equal to max
	 * if input is string or array checks if its size is less that or equal to max
	 * @return bool
	 */
    public function max ($input, $args) {
        if (!c($input, $this->input)) {
            return false;
        }

        $max = $args;
        $value = $this->input[$input];
        $result = true;
        if (is_numeric($value)) {
            $result = $value <= $max;
        }
        if (is_string($value)) {
            $result = strlen($value) <= $max;
        }
        if (is_array($value)) {
            $result = count($value) <= $max;
        }

        if (!$result) {
            $this->addMessage($input, 'max', "$input value is above $max");
        }
        return $result;
    }

	/**
	 * if input is numeric checks if its values is greater than or equal to min
	 * if input is string or array checks if its size is greater that or equal to min
	 * @return bool
	 */
    public function min ($input, $args) {
        if (!c($input, $this->input)) {
            return false;
        }

        $min = $args;
        $value = $this->input[$input];
        $result = true;
        if (is_numeric($value)) {
            $result = $value >= $min;
        }
        if (is_string($value)) {
            $result = strlen($value) >= $min;
        }
        if (is_array($value)) {
            $result = count($value) >= $min;
        }
        if (!$result) {
            $this->addMessage($input, 'min', "$input value is below $min");
        }
        return $result;
    }

	/**
	 * checks if input matches regex
	 * @return bool
	 */
    public function regex ($input, $args) {
        $subject = c($input, $this->input);
        if (!is_string($subject)) {
            $this->addMessage($input, 'regex', "$input must be string");
            return false;
        }
        $subject = trim($subject);
        if (!preg_match($args, $subject)) {
            $this->addMessage($input, 'regex', "$input is in invalid format");
        }
        return true;
    }
}
<?php

defined('EXEC') or die('Restricted access');

require_once LIB_DIR . DS . 'helpers.php';

class View
{
	/**
	 * @var string path to layout
	 */
	private $file;

	/**
	 * @var array variables to use in layout file
	 */
    private $vars = [];

	/**
	 * @param string $file path to layout file
	 * @param array $vars assoc array of variables where key is name of variable to use in
	 * layout files
	 */
    public function __construct ($file, $vars) {
        if (!file_exists($file)) {
            throw new Exception("Could not find file: $file", 1);
        }

        $this->file = $file;
        $this->vars = $vars;
    }

    /**
     * tries to display view, dies if $file doesn't exist
     * 
     * @param string $file - file to display
     * @param array $vars - variables to pass to view to access them in layout files
     */
    public static function displayOrDie ($file, $vars) {
        try {
            $view = new View($file, $vars);
        } catch (Exception $e) {
            echo $e->getMessage();
            die;
        }

        $view->display();
    }

	/**
	 * displays layout file
	 */
    public function display () {
        foreach ($this->vars as $k => $v) {
            if (is_string($k)) {
                $$k = $v;
            }
        }
        require_once $this->file;
    }

    /**
     * get path to partial from partial name
     * @param string $partial - partial name
     * @return string path to partial
     */
    public static function partial ($partial) {
        $path = ASSETS_DIR . DS . 'partials' . DS . $partial;
        return $path;
    }

    /**
     * get path to asset
     * @param string $asset - asset name
     * @return string path to asset
     */
    public static function asset ($asset) {
        $path = ASSETS_DIR . DS . $asset;
        return $path;
    }
}
<?php

defined('EXEC') or die('Restricted access');

require_once LIB_DIR . DS . 'request.php';
require_once LIB_DIR . DS . 'config.php';

abstract class Controller
{
    protected $page;
    protected $task;

    /**
     * redirects to specified url or to fallbackPage
     * @param mixed $url - url to redirect to or null
     */
    protected function redirect($url = '') {
        if (!$url) {
            $url = DS . 'index.php?page=' . self::$fallbackPage;
        }
        header('Location: ' . $url);
        die;
    }

    /**
     * call aproprate controller to handle request
     * @param string $page - page to load, if not passed value will be taken from $_GET
     * @param string $task - task to do, if not passed value will be taken from $_GET
     */
    public static function execute ($page = null, $task = null) {
        $config = Config::getInstance();
        if (!$page) {
            $page = Request::get('page', '');
        }
        if (!$task) {
            $task = Request::get('task', 'index');
        }

        if (file_exists(PAGES_DIR . DS . $page . DS . 'controller.php')) {
            self::execController($page, $task);
        } else {
            self::execController($config->fallbackPage, $task);
        }
    }

	/**
	 * executes method called $task of controller called $page . Controller
	 * if method is not defined executes index
	 */
    private static function execController ($page, $task) {
        $controllerPath = PAGES_DIR . DS . $page . DS . 'controller.php';
        $controllerClass = ucfirst($page) . 'Controller';
        if (!file_exists($controllerPath)) {
            die("page: $page not founcd");
        }

        require_once $controllerPath;
        $controller = new $controllerClass;
        $controller->execTask($task);
    }

	/**
	 * executes method called $task
	 * if method is not defined executes index
	 */
    private function execTask ($task) {
        if (method_exists($this, $task)) {
            $this->{$task}();
        } else {
            $this->index();
        }
    }

	/**
	 * default method that gets called when request method is not defined
	 */
    public abstract function index ();
}
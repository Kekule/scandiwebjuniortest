<?php

defined('EXEC') or die('Restricted access');

/**
 * class for wrapping get and post requests
 */
class Request
{
    /**
     * @return mixed - value from $_GET,
     * if $_GET[$key] is not set returns $default
     */
    public static function get ($key, $default = null) {
        if (isset($_GET[$key])) {
            return $_GET[$key];
        }

        return $default;
    }

    /**
     * @return mixed - value from $_POST,
     * if $_POST[$key] is not set returns $default
     */
    public static function post ($key, $default = null) {
        if (isset($_POST[$key])) {
            return $_POST[$key];
        }

        return $default;
    }
}
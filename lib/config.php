<?php

defined('EXEC') or die('Restricted access');

class Config
{

	/**
	 * @var Config singleton instance
	 */
    private static $instance;

	/**
	 * @var array
	 */
    private $requiredKeys = [
        'db',
        'dbName',
        'dbUser',
        'dbPass',
        'host',
        'fallbackPage'
    ];

    /**
     * @var array $config 
     */
    private $config = [];

	/**
	 * reads config data from .config file
	 * @throws Exception if all reuired keys are not set
	 */
    private function __construct () {
        $file = dirname(__DIR__) . DS . '.config';
        
        if (!file_exists($file)) {
            throw new Exception("File: '$file' counld not be found");
        }

        $this->config = parse_ini_file($file);
        if ($this->config === false) {
            throw new Exception("Could not parse ini string from file: '$file'");
		}
		foreach ($this->requiredKeys as $key) {
			if (!isset($this->config[$key])) {
				throw new Exception("$key not set in config file");
			}
		}
    }

    /**
     * returns instance of Config, creates new one only if it doesn't exist
     * 
     * @return Config - singleton instance of Confg
     */
    public static function getInstance () {
        if (self::$instance === null) {
            try {
                self::$instance = new Config();
            } catch (Exception $e) {
                echo $e->getMessage();
                die;
            }
        }

        return self::$instance;
    }

    /**
     * @return array - all config keys
     */
    public function getKeys () {
        return array_keys($this->config);
    }

    /**
     * @param string $key - key to get value of from config
     * @return mixed - if $key is defined in config than return value of it, null otherwise
     */
    public function __get ($key) {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }

        return null;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed - returns $value
     */
    public function __set ($key, $value) {
        $this->config[$key] = $value;
        return $value;
    }

}
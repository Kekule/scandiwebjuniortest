<?php

defined('EXEC') or die('Restricted access');

require_once LIB_DIR . DS . 'config.php';

/**
 * class for creating PDO object only once per request
 */
class DB
{
	/**
	 * @var DB singleton instance
	 */
	private static $instance;

	/**
	 * @var PDO
	 */
	private $pdo;

	/**
	 * creates pdo object
	 * if error occures while reading configuration prints error message and dies
	 */
	private function __construct () {
		try {
			$config = Config::getInstance();
		} catch (Exception $e) {
			echo $e->getMessage();
			die;
		}

		$dsn = "$config->db:host=$config->host;dbname=$config->dbName;";
		try {
			$this->pdo = new PDO($dsn, $config->dbUser, $config->dbPass);
		} catch (PDOException $e) {
			echo $e->getMessage();
			die;
		}
	}

	/**
	 * @return DB - singleton, creates new one if it doesn't exist
	 */
	public function getInstance () {
		if (!self::$instance) {
			self::$instance = new DB();
		}

		return self::$instance;
	}

	/**
	 * creates new DB if it doesn't exist
	 * @return PDO
	 */
	public static function getPDO () {
		$db = self::getInstance();
		return $db->pdo;
	}

}
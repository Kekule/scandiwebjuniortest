<?php

session_start();

define('EXEC', 1);

define('DS', DIRECTORY_SEPARATOR);
define('LIB_DIR', __DIR__ . DS . 'lib');
define('ROOT_DIR', __DIR__);
define('ASSETS_DIR', __DIR__ . DS . 'assets');
define('PAGES_DIR', __DIR__ . DS . 'pages');
define('PARTIALS_DIR', __DIR__ . DS . 'partials');

require_once LIB_DIR . DS . 'controller.php';

Controller::execute();